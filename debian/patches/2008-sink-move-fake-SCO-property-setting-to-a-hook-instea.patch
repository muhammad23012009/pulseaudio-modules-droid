From ddcb09f06fa91ab06ea507b206f9264b5364faf8 Mon Sep 17 00:00:00 2001
From: Ratchanan Srirattanamet <ratchanan@ubports.com>
Date: Wed, 17 Aug 2022 02:36:53 +0700
Subject: [PATCH] sink: move fake SCO property setting to a hook instead

Pulseaudio calls the sink's set_port callback on IO thread instead of
main thread if "deferred volumes" are in use, and with 65483884 ("sink:
Enable deferred volume for hw volume control"), we're doing just that.
This means we cannot update SCO fake sink's property from that callback,
as an assert will cause the whole PA to crash.

Thus, move the property setting logic into a hook, which is guaranteed
to run on the main thread. Yes, I know that it's a little bit silly to
run hook for our own sink, but that's the way it has to be.

Note that we register a separated hook from the existing `sink_port_
changed_hook_cb`, since that one is used for another purpose and has
some hook/unhook logic which I would rather not touch.

Version 2:
  - Account for the fact that `data->device_port` can be null, presumbly
    when switching to a parking port.

Fixes: https://github.com/ubports/ubuntu-touch/issues/1977
---
 src/droid/droid-sink.c | 35 +++++++++++++++++++++++++++++++----
 1 file changed, 31 insertions(+), 4 deletions(-)

diff --git a/src/droid/droid-sink.c b/src/droid/droid-sink.c
index 68dac78..0b08ca5 100644
--- a/src/droid/droid-sink.c
+++ b/src/droid/droid-sink.c
@@ -105,6 +105,7 @@ struct userdata {
 
     char *sco_fake_sink_name;
     struct pa_sink *sco_fake_sink;
+    pa_hook_slot *sink_port_changed_hook_slot_for_scofakesink;
 };
 
 #define DEFAULT_MODULE_ID "primary"
@@ -528,7 +529,6 @@ static int sink_process_msg(pa_msgobject *o, int code, void *data, int64_t offse
 static int sink_set_port_cb(pa_sink *s, pa_device_port *p) {
     struct userdata *u = s->userdata;
     pa_droid_port_data *data;
-    const char *sco_transport_enabled;
 
     pa_assert(u);
     pa_assert(p);
@@ -546,6 +546,28 @@ static int sink_set_port_cb(pa_sink *s, pa_device_port *p) {
     pa_log_debug("Sink set port %#010x (%s)", data->device_port->type, data->device_port->name);
 
     u->active_device_port = data->device_port;
+    do_routing(u);
+
+    return 0;
+}
+
+/* Done as a hook instead of in the above function since it runs on the IO thread,
+ * and proplist update needs to happen on the main thread. */
+static pa_hook_result_t sink_port_changed_hook_for_scofakesink_cb(pa_core *c, pa_sink *sink, struct userdata *u) {
+    pa_device_port *port;
+    pa_droid_port_data *data;
+    const char *sco_transport_enabled;
+
+    if (sink != u->sink)
+        return PA_HOOK_OK;
+
+    port = sink->active_port;
+    data = PA_DEVICE_PORT_DATA(port);
+
+    if (!data->device_port) {
+        /* Do nothing for parking port. */
+        return PA_HOOK_OK;
+    }
 
     /* See if the sco fake sink element is available (only when needed) */
     if ((u->sco_fake_sink == NULL) && (data->device_port->type & AUDIO_DEVICE_OUT_ALL_SCO))
@@ -561,9 +583,7 @@ static int sink_set_port_cb(pa_sink *s, pa_device_port *p) {
             set_fake_sco_sink_transport_property(u, "true");
     }
 
-    do_routing(u);
-
-    return 0;
+    return PA_HOOK_OK;
 }
 
 static void apply_volume(pa_sink *s) {
@@ -1241,6 +1261,10 @@ pa_sink *pa_droid_sink_new(pa_module *m,
         u->sink->set_port = sink_set_port_cb;
     }
 
+    /* Hooks for setting fake-SCO properties. */
+    u->sink_port_changed_hook_slot_for_scofakesink = pa_hook_connect(&m->core->hooks[PA_CORE_HOOK_SINK_PORT_CHANGED], PA_HOOK_LATE,
+            (pa_hook_cb_t) sink_port_changed_hook_for_scofakesink_cb, u);
+
     update_volumes(u);
 
     pa_droid_stream_suspend(u->stream, false);
@@ -1302,6 +1326,9 @@ static void userdata_free(struct userdata *u) {
     if (u->sink_proplist_changed_hook_slot)
         pa_hook_slot_free(u->sink_proplist_changed_hook_slot);
 
+    if (u->sink_port_changed_hook_slot_for_scofakesink)
+        pa_hook_slot_free(u->sink_port_changed_hook_slot_for_scofakesink);
+
     if (u->sink)
         pa_sink_unref(u->sink);
 
-- 
2.25.1

